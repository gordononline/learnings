from io import StringIO
from IPython.display import Image
from keras.layers import Dense, Activation, Dropout
from keras.models import Sequential
from scikeras.wrappers import KerasClassifier
from numpy.random import seed
from sklearn import metrics
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split 
from sklearn.naive_bayes import GaussianNB
from sklearn.preprocessing import StandardScaler
from sklearn.tree import DecisionTreeClassifier 
from sklearn.tree import export_graphviz
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pydotplus
import random
import ydata_profiling
from autonotebook import tqdm as notebook_tqdm